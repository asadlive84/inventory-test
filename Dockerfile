FROM python:3.6



RUN apt-get update && apt-get install build-essential python-dev -y
RUN pip install psycopg2-binary
COPY requirements.txt /var/www/html/requirements.txt

RUN pip install -r /var/www/html/requirements.txt && pip install uwsgi



COPY . /var/www/html

EXPOSE 9090

WORKDIR /var/www/html

CMD ["uwsgi", "--http", ":9090", "--wsgi-file", "inventory/wsgi.py", "--master", "--processes", "4", "--threads", "2"]