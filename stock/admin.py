from django.contrib import admin
from stock.models import Products, Purchases, Supplier, Sales


admin.site.register(Products)
admin.site.register(Purchases)
admin.site.register(Supplier)
admin.site.register(Sales)
