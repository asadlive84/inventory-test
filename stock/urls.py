from django.urls import path, include
from rest_framework.routers import DefaultRouter
from stock import views

router = DefaultRouter()

router.register(r'products', views.ProductView)
router.register(r'sales', views.SalesView)
router.register(r'purchase', views.PurchaseView)
router.register(r'supplier', views.SupplierView)

urlpatterns = [
    path('', include(router.urls)),

]