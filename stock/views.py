from django.shortcuts import render
from stock.models import Purchases, Products, Supplier, Sales
from stock.api import serializer
from rest_framework import views, generics, viewsets


class ProductView(viewsets.ModelViewSet):
    queryset = Products.objects.all()
    serializer_class = serializer.ProductSerializer


class SupplierView(viewsets.ModelViewSet):
    queryset = Supplier.objects.all()
    serializer_class = serializer.SupplierSerializer


class PurchaseView(viewsets.ModelViewSet):
    queryset = Purchases.objects.all()
    serializer_class = serializer.PurchasesSerializer


class SalesView(viewsets.ModelViewSet):
    queryset = Sales.objects.all()
    serializer_class = serializer.SalesSerializer
