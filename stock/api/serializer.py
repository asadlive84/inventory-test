from stock.models import Purchases, Products, Supplier, Sales
from rest_framework import serializers


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ['id','product_name', 'product_number', 'product_label', 'product_price', 'product_quantity',
                  'product_quantity_hand', 'product_owner', 'product_owner', 'product_received', 'product_shipped',
                  'created_date']
        read_only_field = ('id',)


class SupplierSerializer(serializers.ModelSerializer):
    class Meta:
        model = Supplier
        fields = ['supplier_name', 'supplier_address', 'contact_number', 'email_address']


class PurchasesSerializer(serializers.HyperlinkedModelSerializer):
    product = ProductSerializer(many=True)

    class Meta:
        model = Purchases
        fields = ['purchase_order_no', 'product', 'supplier', 'product_quantity', 'due_taka_status', 'due_taka',
                  'paid_taka']


class SalesSerializer(serializers.ModelSerializer):
    product = ProductSerializer(many=True)

    class Meta:
        model = Sales
        fields = ['sales_no', 'client_name', 'client_address', 'contact_number', 'product', 'product_quantity', 'due_taka_status','due_taka', 'paid_taka', 'created_date']
