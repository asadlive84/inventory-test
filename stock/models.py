from django.db import models


class Products(models.Model):
    product_name = models.CharField("Product Name", max_length=200)
    product_number = models.CharField("Product Number", max_length=200)
    product_label = models.CharField("Product Label", max_length=200)
    product_price = models.IntegerField("Product Price")
    product_quantity = models.IntegerField()
    product_quantity_hand = models.IntegerField(default=0)
    product_owner = models.BooleanField(default=True)
    product_received = models.BooleanField(default=False)
    product_shipped = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.product_name

    class Meta:
        ordering = ['-created_date']


class Supplier(models.Model):
    supplier_name = models.CharField("Supplier or Company Name", max_length=200)
    supplier_address = models.CharField("Supplier/Company Address", max_length=100)
    contact_number = models.CharField("Contact Number", max_length=100)
    email_address = models.EmailField()
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.supplier_name

    class Meta:
        ordering = ['-created_date']


class Purchases(models.Model):
    purchase_order_no = models.CharField("Order No", max_length=100)
    product = models.ManyToManyField(Products)
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE)
    product_quantity = models.IntegerField()
    due_taka_status = models.BooleanField(default=False)
    due_taka = models.IntegerField(default=0)
    paid_taka = models.IntegerField()
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.purchase_order_no

    class Meta:
        ordering = ['-created_date']


class Sales(models.Model):
    sales_no = models.CharField("Sales No", max_length=100)
    client_name = models.CharField("Client Name", max_length=100)
    client_address = models.TextField("Client Address")
    contact_number = models.CharField("Contact Number", max_length=100)
    product = models.ManyToManyField(Products)
    product_quantity = models.IntegerField()
    due_taka_status = models.BooleanField(default=False)
    due_taka = models.IntegerField(default=0)
    paid_taka = models.IntegerField()
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.sales_no

    class Meta:
        ordering = ['-created_date']
