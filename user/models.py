from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager


class CustomUserManager(UserManager):
    pass


class CustomUserModel(AbstractUser):
    objects = CustomUserManager()
